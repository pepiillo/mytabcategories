<?php

if (!defined('_PS_VERSION_')) {
    exit;
}


class MyTabCategories extends Module
{
    const MODULE_NAME = 'mytabcategories';
    const MODULE_VERSION = '0.0.1';
    const MODULE_AUTHOR = 'Jose Gil';

    public function __construct()
    {
        $this->name = self::MODULE_NAME;
        $this->version = self::MODULE_VERSION;
        $this->author = self::MODULE_AUTHOR;
        $this->need_instance = 0;
        parent::__construct();
        $this->displayName = $this->l('Tab Categories');
        $this->description = $this->l('Module creates a new tabs on the backoffice product page');
        $this->ps_versions_compliancy = array('min' => '1.7.0', 'max' => _PS_VERSION_);
    }

    public function install(): bool
    {
        return parent::install();
    }

    public function uninstall(): bool
    {
        return parent::uninstall();
    }

    public function enable($force_all = false)
    {
        return parent::enable($force_all);
    }

    public function disable($force_all = false)
    {
        return parent::disable($force_all);
    }

}

?>
